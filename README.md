# Redirect static page

This simple html page contains the redirect metatag in order to manage the redirect from http://greenc.sciencedesigners.com to http://greenc.sequentiabiotech.com

This was necessary because the host provider GoDaddy is facing issues with the forwarding service.

Documentation:
- https://docs.gitlab.com/ee/user/project/pages/#getting-started
- https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html
- https://medium.com/daniel-canetti/gitlab-pages-f4154134f59d